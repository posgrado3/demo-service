package com.example.demo.controller;

import com.example.demo.entity.Derum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/test")
public class TestController {

  @Autowired private Derum derum;

  @GetMapping
  public String getFullName() {
    return String.format("Welcome %s", derum.getFullName());
  }

}
